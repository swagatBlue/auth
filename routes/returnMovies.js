const {ReturnMovie, validate} = require('../models/returnMovie'); 
const {Movie} = require('../models/movie'); 
const {Customer} = require('../models/customer'); 
//const {Rental} = require('../models/rental');
const mongoose = require('mongoose');
const Fawn = require('fawn');
const express = require('express');
const router = express.Router();

//Fawn.init(mongoose);

router.get('/', async (req, res) => {
  const returnMovies = await ReturnMovie.find().sort('-dateReturned');
  res.send(returnMovies);
});

router.post('/', async (req, res) => {
  const { error } = validate(req.body); 
  if (error) return res.status(400).send(error.details[0].message);

  const customer = await Customer.findById(req.body.customerId);
  if (!customer) return res.status(400).send('Invalid customer.');

  const movie = await Movie.findById(req.body.movieId);
  if (!movie) return res.status(400).send('Invalid movie.');

  /*const rental = await Rental.findById(req.body._id);
  console.log({rental});*/

  let returnMovie = new ReturnMovie({ 
    customer: {
      _id: customer._id,
      name: customer.name, 
      phone: customer.phone
    },
    movie: {
      _id: movie._id,
      title: movie.title,
      dailyRentalRate: movie.dailyRentalRate
    }
  });

  try {
    new Fawn.Task()
      .save('returnMovies', returnMovie)
      .update('movies', { _id: movie._id }, { 
        $inc: { numberInStock: +1 }
      })
      //.remove('rentals', {_id: rental._id} )
      .run();
  
    res.send(returnMovie);
  }
  catch(ex) {
    res.status(500).send('Something failed.');
  }
  //const rental = await Rental.findByIdAndRemove(req.body._id);
});

router.get('/:id', async (req, res) => {
  const returnMovie = await ReturnMovie.findById(req.params.id);

  if (!returnMovie) return res.status(404).send('The rental with the given ID was not found.');

  res.send(returnMovie);
});

module.exports = router;