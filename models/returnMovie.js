const Joi = require('joi');
const mongoose = require('mongoose');

const ReturnMovie = mongoose.model('ReturnMovie', new mongoose.Schema({
  customer: { 
    type: new mongoose.Schema({
      name: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 50
      },
      isGold: {
        type: Boolean,
        default: false
      },
      phone: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 50
      }      
    }),  
    required: true
  },
  movie: {
    type: new mongoose.Schema({
      title: {
        type: String,
        required: true,
        trim: true, 
        minlength: 5,
        maxlength: 255
      },
      dailyRentalRate: { 
        type: Number, 
        required: true,
        min: 0,
        max: 255
      }   
    }),
    required: true
  },
  dateOut: { 
    type: Date
  },
  dateReturned: { 
    type: Date, 
    required: true,
    default: Date.now
  },
  rentalFee: { 
    type: Number, 
    min: 0
  }
}));

function validateReturnMovie(returnMovie) {
  const schema = {
    customerId: Joi.objectId().required(),
    movieId: Joi.objectId().required()
  };

  return Joi.validate(returnMovie, schema);
}

exports.ReturnMovie = ReturnMovie; 
exports.validate = validateReturnMovie;